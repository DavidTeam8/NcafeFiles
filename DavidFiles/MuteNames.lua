local function MuteNames(msg)
if msg.content.text then
text = msg.content.text.text
end
--- Start Code ---
if DevRio:get(David.."Rio:Lock:MuteNames"..msg.chat_id) then
if Manager(msg) then
if text and (text:match("^كتم اسم (.*)$") or text:match("^كتم الاسم (.*)$")) then
local MuteName = text:match("^كتم اسم (.*)$") or text:match("^كتم الاسم (.*)$")
LuaTele.sendText(msg.chat_id, msg.id, '↯︙تم كتم الاسم ↫ '..MuteName)
DevRio:sadd(David.."Rio:Mute:Names"..msg.chat_id, MuteName)
end
if text and (text:match("^الغاء كتم اسم (.*)$") or text:match("^الغاء كتم الاسم (.*)$")) then
local UnMuteName = text:match("^الغاء كتم اسم (.*)$") or text:match("^الغاء كتم الاسم (.*)$")
LuaTele.sendText(msg.chat_id, msg.id, '↯︙تم الغاء كتم الاسم ↫ '..UnMuteName)
DevRio:srem(David.."Rio:Mute:Names"..msg.chat_id, UnMuteName)
end
end
if text == "مسح الاسماء المكتومه" and Constructor(msg) then
DevRio:del(David.."Rio:Mute:Names"..msg.chat_id)
LuaTele.sendText(msg.chat_id, msg.id, "↯︙تم مسح الاسماء المكتومه")
end
if text == "الاسماء المكتومه" and Constructor(msg) then
local AllNames = DevRio:smembers(David.."Rio:Mute:Names"..msg.chat_id)
Text = "\n↯︙قائمة الاسماء المكتومه ↫ ⤈\n┉ ┉ ┉ ┉ ┉ ┉ ┉ ┉ ┉\n"
for k,v in pairs(AllNames) do
Text = Text..""..k.."~ : (["..v.."])\n"
end
if #AllNames == 0 then
Text = "↯︙لاتوجد اسماء مكتومه"
end
LuaTele.sendText(msg.chat_id, msg.id, Text,'md')
end
end
if not Manager(msg) and DevRio:get(David.."Rio:Lock:MuteNames"..msg.chat_id) then
local result = LuaTele.getUser(msg.sender_id.user_id)
if result.id then 
DavidName = ((result.first_name or "") .. (result.last_name or ""))
if DavidName then 
DavidNames = DevRio:smembers(David.."Rio:Mute:Names"..msg.chat_id) or ""
if DavidNames and DavidNames[1] then 
for i=1,#DavidNames do 
if DavidName:match("(.*)("..DavidNames[i]..")(.*)") then 
LuaTele.deleteMessages(msg.chat_id,{[1]= msg.id})
end
end
end
end
end
end
if Constructor(msg) then
if text == "تفعيل كتم الاسم" or text == "تفعيل كتم الاسماء" then
LuaTele.sendText(msg.chat_id, msg.id, '↯︙تم التفعيل سيتم كتم العضو الذي يضع الاسماء المكتومه')
DevRio:set(David.."Rio:Lock:MuteNames"..msg.chat_id,true)
end
if text == "تعطيل كتم الاسم" or text == "تعطيل كتم الاسماء" then
LuaTele.sendText(msg.chat_id, msg.id, '↯︙تم تعطيل سيتم كتم العضو الذي يضع الاسماء المكتومه')
DevRio:del(David.."Rio:Lock:MuteNames"..msg.chat_id)
end
end
--- End Function ---
end
return {David = MuteNames}
